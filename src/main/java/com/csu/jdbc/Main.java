package com.csu.jdbc;

import com.csu.jdbc.dao.UserRepository;
import com.csu.jdbc.dao.UserRepositoryImpl;
import com.csu.jdbc.schema.User;

public class Main {
    public static void main(String[] args) {
        UserRepository userRepository = new UserRepositoryImpl();

        userRepository.save(User
                .builder()
                        .lastName("Ivan")
                        .firstName("Ivanov")
                        .address("Street")
                        .city("Chelyabinsk")
                .build());
    }

}
