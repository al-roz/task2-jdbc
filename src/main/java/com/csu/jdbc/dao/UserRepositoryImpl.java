package com.csu.jdbc.dao;

import com.csu.jdbc.schema.User;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class UserRepositoryImpl implements UserRepository {

    String url = "jdbc:mysql://localhost:3306/lab_db";
    String username = "root";
    String password = "example";

    static {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getUserById(Long id) {
        String lastName = null;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            var statement = conn.prepareStatement("SELECT last_name, first_name, id FROM Users WHERE id = ?");
            statement.setLong(1, id);
            var result = statement.executeQuery();

            while (result.next()) {
                lastName = result.getString("last_name");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lastName;
    }

    @Override
    public String getUserByFirstName(String name) {
        return null;
    }

    @Override
    public String getUserByLastName(String name) {
        return null;
    }

    @Override
    public void save(User user) {
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            var statement = conn
                    .prepareStatement("INSERT INTO Users (last_name, first_name, address, city) VALUES (?, ?, ?, ?)");
            statement.setString(1, user.getLastName());
            statement.setString(2, user.getFirstName());
            statement.setString(3, user.getAddress());
            statement.setString(4, user.getCity());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
